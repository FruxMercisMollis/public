# C sharp and Dot Net Core notes

[Cartella Drive](https://drive.google.com/drive/folders/1sS54Mm4JWFhhKJqY3aREJ4rF4wx_mSfC)

Installazioni necessarie:
- Visual Studio (TO-DO: aggiungere configurazione)
- Microsoft SQL Server
- Microsoft SQL Server Management Studio

Dot Net Core nasce nel 2019 per essere multipiattaforma a differenza di Dot Net Framework (web form).

## 23/09/2024

L'applicazione web è contenuta all'interno di una cartella per essere esposta verso l'esterno: IIS (internet information services).
Altri esempi: tomcat.

HTML: linguaggio descrittivo.

Crawler, SEO, ranching

### Creare un progetto dot net core

1. Aprire Visual Studio
2. Crea un nuovo progetto
3) App Web ASP.NET Core
    3.1) selezionare il percorso che preferiamo (una cartella qualsiasi)
    3.2) flaggare "Inserisci soluzione e progetto nella stessa directory"

### Struttura del progetto

![https button](./Src/Img/struttura_directory.png)

- `/Pages` fa da contenitore di tutte le pagine.
- Ogni pagina è formata da:
    - una parte grafica --> `.cshtml` (oltre all'html contiene del codice c#)
    - da una parte di codice (code behind) --> file `.cshtml.cs`
- `/Pages/Shared` contiene i file condividisi dall'intera applicazione
- `/Pages/_ViewStart.cshtml` indica quale è la pagina (cioè quale file .cshtml) in cui iniettare tutte le pagine dell'applicazione (master page). Di default:

```
@{
    Layout = "_Layout";
}
```

- `/Pages/Shared/_Layout.cshtml` è la pagina principale in cui iniettare il codice html di tutte le pagine dell'applicazione. Il punto esatto della pagina in cui avviene questa iniezione è in corrispondenza del tag `@RenderBody()`:

```html
    <div class="container">
        <main role="main" class="pb-3">
            @RenderBody()
        </main>
    </div>
```

- `/Pages/Index.cshtml` body della pagina principale (home page).


### Prima esecuzione

Premendo il pulsante:

![https button](./Src/Img/1-https_button.png)

si manda in esecuzione l'applicazione web, ottenendo:

![index page](./Src/Img/1-index.png)


### Introduzione a Razor

**Razor** = view engine per creare pagine html usando codice c#

Il codice "razor" viene mescolato nell'html con un'apposita sintassi che inizi con `@`. Esempio:

```html
// Pages/Index.cshtml

@page
@model IndexModel
@{
    ViewData["Title"] = "Home page";
    ViewData["Intestazione"] = "Titolo della pagina";
}

@{
    var nome = "Antonio";
}

@if(1 == 1)
{
    <p>Ciao @nome</p>
}

<div class="text-center">
    <h1 class="display-4">@ViewData["Intestazione"]</h1>
    <p>Learn about <a href="https://learn.microsoft.com/aspnet/core">building Web apps with ASP.NET Core</a>.</p>
</div>
```

Ogni pagina creata, nella sua parte di codice (file `.cshtml.cs`) contiene il metodo `onGet()`. Ogni volta che la pagina verrà visualizzata, verrà eseguito questo metodo. Esempio:

```c#
// Pages/Index.cshtml.cs

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ProgettoProva1.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
        
        }
    }
}
```

### Creare una nuova pagina

1. Cliccare con il tasto destro sulla cartella "Pages"
2. Aggiungi > Pagina razor > Pagina razor vuota
3. Dare un nome alla pagina

Verranno creati i due classici file:
- NomePagina`.cshtml` (pagina "frontend" razor)
- NomePagina`.cshtml.cs` ("backend")

Per accedere alla pagina creata occorre aggiungere un qualche pulsante nella pagina `Index.cshtml`. Ad esempio:

```html
    <li class="nav-item">
        <a class="nav-link text-dark" asp-area="" asp-page="/PaginaProva">Pagina di prova</a>
    </li>
```

### Creare un modello (cioè una classe)

1. creare la cartella "Models"
    1.1) cliccare con il tasto destro sul nome del progetto
    1.2) Aggiungi > Nuova cartella
2. creare una nuova classe
    2.1) cliccare con il tasto destro sulla cartella "Models"
    2.2) Aggiungi > Classe
    2.3) Inserire il nome della classe

Esempio della classe `Persona`:

```c#
// Models/Persona.cs

namespace ProgettoProva1.Models
{
    public class Persona
    {
        public int anni {  get; set; }
        public string? nome { get; set; } // nullable
    }
}

```

**Esempio form**:

```html
// Pages/PaginaProva.cshtml

@page
@model ProgettoProva1.Pages.PaginaProvaModel
@{
}

<p>Ciao a tutti, io sono una pagina di prova</p>

<div>
    <form method="post">

        <label>Nome</label>
        <input asp-for="personaNuova.nome"/>
        <br />

        <label>Eta</label>
        <input type="number" asp-for="personaNuova.anni" />
        <br />
        
        <button type="submit">Invia i dati</button>

    </form>
</div>
```

**Esempio backend**:

```c#
// Pages/PaginaProva.cshtml.cs

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using ProgettoProva1.Models;

namespace ProgettoProva1.Pages
{

    public class PaginaProvaModel : PageModel
    {
        [BindProperty] // consente il bind dei dati tra backend e frontend
        public Persona personaNuova { get; set; } = default!;
        public void OnGet()
        {
            // inizializza i campi del form con i valori specificati
            personaNuova = new Persona();
            personaNuova.anni = 58;
            personaNuova.nome = "Filiberto";
        }

        public void onPost()
        {
            /// [???] Non so come stampare i dati inviati dal form
        }
    }
}
```

## 24/09/2024

