var days = document.getElementById('days');

var silverdouble = document.getElementById('silver-double');
var silverdoubleuse = document.getElementById('silver-double_use');
var labelsilverdoubleuse = document.getElementById('label-silver-double_use');
var silversingleuse = document.getElementById('silver-single_use');
var labelsilversingleuse = document.getElementById('label-silver-single_use');

var silverbed2 = document.getElementById('silver-bed2');
var labelsilverbed2 = document.getElementById('label-silver-bed2');
var labelsilverbed2age = document.getElementById('label-silver-bed2-age');
var silverbed2age = document.getElementById('silver-bed2-age');

var silverbed3 = document.getElementById('silver-bed3');
var labelsilverbed3 = document.getElementById('label-silver-bed3');
var labelsilverbed3age = document.getElementById('label-silver-bed3-age');
var silverbed3age = document.getElementById('silver-bed3-age');

var btntotal = document.getElementById('btntotal');
var total = document.getElementById('total');

silverdouble.addEventListener('click', function() {
    if(silverdouble.checked) {
        silverdoubleuse.classList.remove('visibility-hidden');
        silversingleuse.classList.remove('visibility-hidden');
        labelsilverdoubleuse.classList.remove('visibility-hidden');
        labelsilversingleuse.classList.remove('visibility-hidden');

        silverbed2.classList.remove('visibility-hidden');
        labelsilverbed2.classList.remove('visibility-hidden');
        // labelsilverbed2age.classList.remove('visibility-hidden');
        // silverbed2age.classList.remove('visibility-hidden');

        // silverbed3.classList.remove('visibility-hidden');
        // labelsilverbed3.classList.remove('visibility-hidden');
        // labelsilverbed3age.classList.remove('visibility-hidden');
        // silverbed3age.classList.remove('visibility-hidden');

    } else {

        silverdoubleuse.classList.add('visibility-hidden');
        silversingleuse.classList.add('visibility-hidden');
        labelsilverdoubleuse.classList.add('visibility-hidden');
        labelsilversingleuse.classList.add('visibility-hidden');

        silverbed2.classList.add('visibility-hidden');
        labelsilverbed2.classList.add('visibility-hidden');
        labelsilverbed2age.classList.add('visibility-hidden');
        silverbed2age.classList.add('visibility-hidden');
        silverbed2age.value = '';
        silverbed2.checked = false;

        silverbed3.classList.add('visibility-hidden');
        labelsilverbed3.classList.add('visibility-hidden');
        labelsilverbed3age.classList.add('visibility-hidden');
        silverbed3age.classList.add('visibility-hidden');
        silverbed3age.value = '';
        silverbed3.checked = false;
    }
});


silverbed2.addEventListener('click', function(e) {
    if(silverbed2.checked) {
        labelsilverbed2age.classList.remove('visibility-hidden');
        silverbed2age.classList.remove('visibility-hidden');

        silverbed3.classList.remove('visibility-hidden');
        labelsilverbed3.classList.remove('visibility-hidden');
        // labelsilverbed3age.classList.remove('visibility-hidden');
        // silverbed3age.classList.remove('visibility-hidden');

    } else {
        silverbed2age.value = '';
        labelsilverbed2age.classList.add('visibility-hidden');
        silverbed2age.classList.add('visibility-hidden');

        silverbed3.classList.add('visibility-hidden');
        labelsilverbed3.classList.add('visibility-hidden');
        labelsilverbed3age.classList.add('visibility-hidden');
        silverbed3age.classList.add('visibility-hidden');
        silverbed3age.value = '';
        silverbed3.checked = false;
    }
});

silverbed3.addEventListener('click', function(e) {
    if(silverbed3.checked) {
        labelsilverbed3age.classList.remove('visibility-hidden');
        silverbed3age.classList.remove('visibility-hidden');
    } else {
        silverbed3age.value = '';
        labelsilverbed3age.classList.add('visibility-hidden');
        silverbed3age.classList.add('visibility-hidden');
    }
});


btntotal.addEventListener('click', function() {
    var notti = days.value;
    var letto1 = silverdouble.checked;
    var letto2 = silverbed2.checked;
    var eta2 = silverbed2age.value;
    var letto3 = silverbed3.checked;
    var eta3 = silverbed3age.value;

    var tot = 0;

    if(notti != "" && notti > 0 && notti < 15) {

        if(letto1) {
            if(silverdoubleuse.checked) {  // uso matrioniale
                tot += 100;
            } else {  // uso singolo
                tot += 80;
            }

            if(letto2) {
                if(eta2 != "") {
                    if(eta2 > 17) {
                        tot += 40;
                    } else {
                        tot += 20;
                    }
                } else {
                    alert('Inserire età dell\'ospite.');
                }
            }

            if(letto3) {
                if(eta3 != "") {
                    if(eta3 > 17) {
                        tot += 30;
                    } else {
                        tot += 15;
                    }
                } else {
                    alert('Inserire età dell\'ospite.');
                }
            }

        }
    } else {
        alert("Inserire il numero di notti.")
    }

    tot = tot * notti;
    if(tot > 0) {
        total.innerText = tot;        
    } else {
        total.innerText = "";
    }

})
